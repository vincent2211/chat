const express = require("express");
const axios = require("axios");
const bodyParser = require("body-parser");
const morgan = require("morgan");
const mysql = require("mysql");
const logger = require("./rest-mysql/middleware/logger");

var app = express();

//Init Middleware
app.use(logger);

//Middlewares
app.use(morgan("dev"));
app.use(bodyParser.json());

//Setup
app.use(bodyParser.urlencoded({ extended: false }));

//Routes
require("./rest-mysql/src/routes/usersRoutes")(app);
//PORT in ENV or 5000 in default
const PORT = process.env.PORT || 8051;

app.listen(PORT, () => console.log(`API started on port ${PORT}`));
