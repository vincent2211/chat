CREATE DATABASE chatmysql;

USE chatmysql;

CREATE TABLE IF NOT EXISTS `users`(
    `id` INT(10) unsigned NOT NULL AUTO_INCREMENT,
    `is_admin` TINYINT(1)COLLATE utf8_unicode_ci NOT NULL,
    `nickname` VARCHAR(50) COLLATE utf8_unicode_ci NOT NULL,
    `nom`VARCHAR(50) COLLATE utf8_unicode_ci NOT NULL,
    `prenom`VARCHAR(50) COLLATE utf8_unicode_ci NOT NULL,
    `email`VARCHAR(100) COLLATE utf8_unicode_ci NOT NULL,
    `password`VARCHAR(200) COLLATE utf8_unicode_ci NOT NULL,
    `created_at`TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    `updated_at`TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY(`id`),
    UNIQUE KEY `users_email_unique` (`email`)

)ENGINE=InnoDB DEFAULT CHARACTER SET= utf8;



DESCRIBE users;


CREATE TABLE IF NOT EXISTS `messages`(
`id` INT AUTO_INCREMENT,
   `user_id` INT,
   `body` TEXT,
   `date` DATETIME DEFAULT CURRENT_TIMESTAMP,
   PRIMARY KEY(id),
   FOREIGN KEY (user_id) REFERENCES users(id)
);

DESCRIBE messages;

