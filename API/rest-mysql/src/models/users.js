const mysql = require("mysql");

connection = mysql.createConnection({
  host: "localhost",
  user: "root",
  password: "wolf98",
  database: "chatmysql"
});

//Show Users
let userModel = {};

userModel.getUsers = callback => {
  if (connection) {
    connection.query("SELECT * FROM users ORDER BY id", (err, rows) => {
      if (err) {
        throw err;
      } else {
        callback(null, rows);
      }
    });
  }
};

//Create User
userModel.createtUser = (userData, callback) => {
  if (connection) {
    console.log(userData);
    console.log("Ta mamoooooon");
    connection.query("INSERT INTO users SET ?", userData, (err, res) => {
      if (err) {
        console.log("Ta maman");
        throw err;
      } else {
        callback(null, { "Insert user Id": res.insertId }); // error
      }
    });
  }
};
//Update User
userModel.updateUser = (userData, callback) => {
  if (connection) {
    const sql = `
    UPDATE users SET
    nickname = ${connection.escape(userData.nickname)},
    password = ${connection.escape(userData.password)},
    email = ${connection.escape(userData.email)},
    WHERE id = ${connection.escape(userData.id)}
    `;
    connection.query(sql, (err, res) => {
      if (err) {
        throw err;
      } else {
        callback(null, {
          msg: "The User is Updated and ready to Chat"
        });
      }
    });
  }
};
//Delete Users
userModel.deleteUser = (id, callback) => {
  if (connection) {
    const sql = `
    SELECT * FROM users WHERE id = ${connection.escape(id)}
     `;

    connection.query(sql, (err, row) => {
      if (row) {
        const sql = `
         DELETE FROM users WHERE id = ${id}
         `;
        connection.query(sql, (err, res) => {
          if (err) {
            throw err;
          } else {
            callback(null, {
              msg: "deleted"
            });
          }
        });
      } else {
        callback(null, {
          msg: `the user id: ${id} do not exist`
        });
      }
    });
  }
};

module.exports = userModel;
