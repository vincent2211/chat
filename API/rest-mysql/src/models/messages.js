const mysql = require("mysql");

connection = mysql.createConnection({
  host: "localhost",
  user: "root",
  database: "chatmysql"
});

//Show messages
let messageModel = {};

messageModel.getMessages = callback => {
  if (connection) {
    connection.query("SELECT * FROM users ORDER BY id", (err, rows) => {
      if (err) {
        throw err;
      } else {
        callback(null, rows);
      }
    });
  }
};

//Create Message
messageModel.createtMessage = (userData, callback) => {
  if (connection) {
    connection.query("INSERT INTO messages SET ?", userData, (err, res) => {
      if (err) {
        throw err;
      } else {
        callback(null, { "Insert message Id": res.insertId });
      }
    });
  }
};

userModel.deleteMessage = (id, callback) => {
  if (connection) {
    const sql = `
    SELECT * FROM messages WHERE id = ${connection.escape(id)}
     `;

    connection.query(sql, (err, row) => {
      if (row) {
        const sql = `
         DELETE FROM messages WHERE id = ${id}
         `;
        connection.query(sql, (err, res) => {
          if (err) {
            throw err;
          } else {
            callback(null, {
              msg: "deleted"
            });
          }
        });
      } else {
        callback(null, {
          msg: `the user id: ${id} do not exist`
        });
      }
    });
  }
};

module.exports = messageModel;
