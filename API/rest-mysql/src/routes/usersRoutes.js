const user = require("../models/users");

module.exports = function(app) {
  //Show Users
  app.get("/users", (req, res) => {
    user.getUsers((err, data) => {
      res.json(data);
    });
  });

  //Create a User Parse
  app.post("/users", (req, res) => {
    const userData = {
      id: null,
      is_admin: 0,
      nickname: req.body.nickname,
      nom: req.body.nom,
      prenom: req.body.prenom,
      email: req.body.email,
      pswd: req.body.pswd,
      created_at: null,
      updated_at: null
    };

    console.log("tu mam");
    console.log(userData);

    user.createtUser(userData, (err, data) => {
      if (data && data.insertid) {
        res.json({
          sucess: true,
          msg: `Welcome ${userData.nickname} to the chat`,
          data: data
        });
      } else {
        res.status(500).json({
          success: false,
          msg: "Error"
        });
      }
    });
  });
  //Update User
  app.put("/users/:id", (req, res) => {
    const userData = {
      id: req.params.id,
      nickname: req.body.nickname,
      nom: req.body.nom,
      prenom: req.body.prenom,
      email: req.body.email,
      pswd: req.body.password,
      created_at: null,
      updated_at: null
    };
    user.updateUser(userData, (err, data) => {
      if (data && data.msg) {
        res.json(data);
      } else {
        res.json({
          success: false,
          msg: "error"
        });
      }
    });
  });
  //Delete Users
  app.delete("/users/:id", (req, res) => {
    user.deleteUser(req.params.id, (err, data) => {
      if (
        (data && data.msg === "deleted") ||
        data.msg === `the user id: ${id} do not exist`
      ) {
        res.json({
          success: true,
          data
        });
      } else {
        res.status(500).json({
          msg: "Error"
        });
      }
    });
  });
};
