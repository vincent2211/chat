const user = require("../models/messages");

module.exports = function(app) {
  //Show Messages
  app.get("/msg", (req, res) => {
    user.getMessages((err, data) => {
      res.json(data);
    });
  });

  //Create a Message
  app.post("/msg", (req, res) => {
    const messageData = {
      id: null,
      nickname: req.body.nickname,
      nom: req.body.nom,
      prenom: req.body.prenom,
      email: req.body.email,
      password: req.body.password,
      created_at: null,
      updated_at: null
    };
    user.createtMessage(messageData, (err, data) => {
      if (data && data.insertid) {
        res.json({
          sucess: true,
          msg: `Welcome ${messageData.nickname} to the chat`,
          data: data
        });
      } else {
        res.status(500).json({
          success: false,
          msg: "Error"
        });
      }
    });
  });

  //Delete Message
  app.delete("/msg/:id", (req, res) => {
    user.deleteUser(req.params.id, (err, data) => {
      if (
        (data && data.msg === "deleted") ||
        data.msg === `the message with the id: ${id} do not exist`
      ) {
        res.json({
          success: true,
          data
        });
      } else {
        res.status(500).json({
          msg: "Error"
        });
      }
    });
  });
};
