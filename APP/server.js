var express = require("express");
var axios = require("axios");
const path = require("path");
var bodyParser = require("body-parser");
const logger = require("./middleware/logger");

var app = express();

//Init Middlewere
app.use(logger);

//Endpoint
//Static folder
app.use(express.static(path.join(__dirname, "public")));

//PORT in ENV or 5000 in default
const PORT = process.env.PORT || 5000;

app.listen(PORT, () => console.log(`server started on port ${PORT}`));
